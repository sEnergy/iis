$(function() {
    $(".alert button.close").click(function() {
        var div = $(this).closest("div.alert");
        $(div).contents().hide("slide", function() {
            $(div).hide("slide", {direction: "up"});
        });

    });

    $.nette.init();
    
    $(".datepicker").each(function(idx, elem) {
        $(elem).datepicker({
            changeYear: true,
            showButtonPanel: true,
            yearRange: "-100:+2"
        });
    });
    $(".datetimepicker").each(function(idx, elem) {
        $(elem).datetimepicker();
    });
    Date.prototype.toPHPString = function() {
        return this.toString("dd.MM.yyyy HH:mm");
    };
    
    $.extend(true, $.fn.dataTable.defaults.oLanguage, csLang);
    
    $.fn.dataTable.defaults.fnDrawCallback = function(oSettings){
        $(oSettings.nTableWrapper).find("select").selectpicker({
            width: "auto",
        });
    };
    
    
    $('select').selectpicker();
    
});
