<?php

use jb\Form\Form, 
 Facade\BaseFacade,
 Entity\Treatment;

class TreatmentPresenter extends AdminPresenter {
    
    /** @var BaseFacade */
    protected $treatmentFacade;
    
    public function renderDefault() {
        $this->template->treatments = $this->treatmentFacade->getAllCurrent();
    }
    
    public function renderArchive() {
        $this->template->treatments = $this->treatmentFacade->getAllArchived();
    }
    
    public function renderEdit($id) {
        $treatment = $this->treatmentFacade->get($id);
        $this->redirectIfRemoved($treatment);
        
        $this->template->treatment = $treatment;
        
        $this["editTreatmentForm"]->setDefaults($treatment->toArray());
    }
    
    public function createComponentAddTreatmentForm() {
        $form = new Form();
        
        $this->generateTreatmentFormBody($form);
        
        $form->addSubmit("ok", "Uložit");
        
        $form->onSuccess[] = $this->submittedAddTreatmentForm;
        
        return $form;
    }
    
    public function submittedAddTreatmentForm(Form $form) {
        $values = $form->getValues();
        
        $treatment = new Treatment();
        $treatment->setValues($values);
        
        $this->treatmentFacade->create($treatment);
        
        $this->flashMessage("Zákrok uložen.", "success");
        $this->redirect("Treatment:default");
    }
    
    public function createComponentEditTreatmentForm() {
        $form = new Form();
        
        $this->generateTreatmentFormBody($form);
        
        $form->addHidden("id");
              
        $form->addSubmit("ok", "Uložit");
        
        $form->onSuccess[] = $this->submittedEditTreatmentForm;
        
        return $form;
    }
    
    public function submittedEditTreatmentForm(Form $form) { 
        $values = $form->getValues();
        $treatment = $this->treatmentFacade->get($values->id);
        
        $treatment->setValues($values);
        $this->treatmentFacade->update($treatment);
        
        $this->flashMessage("Zákrok odstraněn.", "success");
        $this->redirect("Treatment:default");
    }
    
    private function generateTreatmentFormBody($form) {       
        $form->addText("name", "Název zákroku")
                ->setRequired()
                ->addRule(Form::MAX_LENGTH, 'Název zákroku je příliš dlouhý.', 50);
        
        $form->addText("expirationInMonths", "Doba účnnosti / opakovat za")
                ->addRule(Form::PATTERN, "Zadejte dobu v měsísích, nebo nevplňujte.", "[0-9]{0,10}");
    }
    
    public function actionRemove($id, $redirectToDestination = null, $redirectArgs = array()) {
        $treatment = $this->treatmentFacade->get($id);
        $this->redirectIfRemoved($treatment);
     
        $treatment->removed = true;
        $this->treatmentFacade->update($treatment);
        
        $this->flashMessage("Zákrok $treatment->name odstraněn z poskytovaných zákroků. Uložen do složky Archiv zákroků.", "success");
        $redirectToDestination = ($redirectToDestination)? $redirectToDestination: "default";
        $this->redirect($redirectToDestination, $redirectArgs);
    } 
    
    public function actionRestore($id, $redirectToDestination = null, $redirectArgs = array()) {
        $treatment = $this->treatmentFacade->get($id);
     
        $treatment->removed = false;
        $this->treatmentFacade->update($treatment);
        
        $this->flashMessage("Zákrok $treatment->name navrácen do složky poskytovaných zákroků.", "success");
        $redirectToDestination = ($redirectToDestination)? $redirectToDestination: "default";
        $this->redirect($redirectToDestination, $redirectArgs);
    }
} 
