<?php

use jb\Form\Form, 
 Facade\BaseFacade,
 Entity\Employee;

class EmployeePresenter extends AdminPresenter {
    
    protected $employeeFacade;
    
    public function renderDefault() {
        $this->template->employees = $this->employeeFacade->getAll();
    }
    
    public function renderEdit($id) {
        $employee = $this->employeeFacade->get($id);
        $this->redirectIfRemoved($employee);
        
        $this["editEmployeeForm"]->setDefaults($employee->toArray());
    }
    
    public function renderEditPassword($id) {
        $this->template->employee = $this->employeeFacade->get($id);
        $this->redirectIfRemoved($this->template->employee);
        
        $this["editEmployeePasswordForm"]->setDefaults($this->template->employee->toArray());
    }
    
    public function actionEdit($id) {
        $employee = $this->employeeFacade->get($id);
        $this->redirectIfRemoved($employee);
    }
    
    public function actionEditPassword($id) {
       $employee = $this->employeeFacade->get($id);
       $this->redirectIfRemoved($employee);
    } 
    
    public function createComponentAddEmployeeForm() {
        $form = new Form();
        
        $this->generateEmployeeFormBody($form);
        
        $form->addPassword("password", "Heslo")
                ->setRequired()
                ->addRule(Form::MIN_LENGTH, 'Heslo musí být alespoň 8 znaků dlouhé', 8);
        
        $form->addPassword("password1", "Heslo znovu")
                ->addRule(Form::EQUAL, "Hesla se musí shodovat", $form["password"]);
        
        $form->addSubmit("ok", "Uložit");
        $form->addButton('cancel', 'Zrušit')
                ->setAttribute('onclick', 'DefaultRedirect()');
        
        $form->onSuccess[] = $this->submittedAddEmployeeForm;
        
        return $form;
    }
    
    public function createComponentEditEmployeeForm() {
        $form = new Form();
        
        $this->generateEmployeeFormBody($form);
        
        $form->addHidden("id");
        
        $form->addSubmit("ok", "Uložit");
        $form->addButton('cancel', 'Zrušit')
                ->setAttribute('onclick', 'DefaultRedirect()');
        
        $form->onSuccess[] = $this->submittedEditEmployeeForm;
        
        return $form;
    }
    
    public function createComponentEditEmployeePasswordForm() {
        $form = new Form();
        
        $form->addHidden("id");
        
        $form->addText("name", "Změnit heslo u")
                ->setDisabled();
        
        $form->addPassword("password", "Nové heslo")
                ->setRequired()
                ->addRule(Form::MIN_LENGTH, 'Heslo musí být alespoň 8 znaků dlouhé', 8);
        
        $form->addPassword("password1", "Nové heslo znovu")
                ->addRule(Form::EQUAL, "Hesla se musí shodovat", $form["password"]);
        
        $form->addSubmit("ok", "Uložit");
        $form->addButton('raise', 'Zrušit')
                ->setAttribute('onclick', 'DefaultRedirect()');
        
        $form->onSuccess[] = $this->submittedEditEmployeeForm;
        
        return $form;
    }
    
    private function generateEmployeeFormBody($form) {
        $form->addText("name", "Jméno")
                ->addRule(Form::MAX_LENGTH, 'Jméno je příliš dlouhé', 50)
                ->setRequired();
        
        $form->addText("username", "Uživatelské jméno")
                ->addRule(Form::MAX_LENGTH, 'Uživatelské jméno je příliš dlouhé', 50)
                ->setRequired();
        
        $form->addSelect("role", "Pozice", Employee::getRoles())
                ->setRequired()
                ->setPrompt("vyberte pozici");
        
        $form->addText("phone", "Telefon")
                ->addRule(Form::PATTERN, "Zadejte telefonnní číslo bez speciálních znaků a mezer, např. 728123456", "[0-9]{9}");
        
        $form->onValidate[] = $this->validateEmployeeFormBody;
    }
    
    public function validateEmployeeFormBody(Form $form) {
    
        $errorMessageEnd = " je již použito jiným zaměstnancem.";
        
        $this->employeeFacade->validateProperty($form, "username", "Zadané uživatelské jméno" . $errorMessageEnd);
        $this->employeeFacade->validateProperty($form, "phone", "Zadané telefonní číslo" . $errorMessageEnd);
    }
    
    public function submittedAddEmployeeForm(Form $form) {
        $values = $form->getValues();
        $values->password = $this->calculateHash($values->password);
        
        $employee = new Employee();
        $employee->setValues($values);
        
        $this->employeeFacade->create($employee);
        
        $this->flashMessage("Zaměstnanec $employee->name úspěšně přidán.", "success");
        $this->redirect("default");
    }
    
    public function submittedEditEmployeeForm(Form $form) {
        
        $values = $form->getValues();
        $employee = $this->employeeFacade->get($values->id);
        
        if (isset($values->password )){
            if ($values->password == "") {
                unset($values->password);
            }
            else {
                $values->password = $this->calculateHash($values->password);
            }
        }
        else {
            $values->password = $employee->password;
        }
        
        $employee->setValues($values);
        $this->employeeFacade->update($employee);
        
        $this->flashMessage($employee->name . " úspěšně upraven.", "success");
        $this->redirect("default");
    }
    
    public function actionRemove($id, $redirectToDestination = null, $redirectArgs = array()) {
        $employee = $this->employeeFacade->get($id);
        $this->redirectIfRemoved($employee);
        
        if ($this->getUser()->getId() == $id){
            $this->flashMessage("Nemůžete odstranit sami sebe!");
            $redirectToDestination = ($redirectToDestination)? $redirectToDestination: "default";
            $this->redirect($redirectToDestination, $redirectArgs);
        }
        
        $this->employeeFacade->remove($employee);
        
        $this->flashMessage($employee->name . " úspěšně odstraněn.", "success");
        $redirectToDestination = ($redirectToDestination)? $redirectToDestination: "default";
        $this->redirect($redirectToDestination, $redirectArgs);
    }
    
} 
