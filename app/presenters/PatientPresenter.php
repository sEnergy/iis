<?php

use jb\Form\Form, 
 Facade\BaseFacade,
 Entity\Patient
        ;

class PatientPresenter extends DoctorPresenter {
    
    /** @var BaseFacade */
    protected $patientFacade;
    
    /** @var BaseFacade */
    protected $insuranceFacade;
    
    public function renderDefault() {
        $this->template->patients = $this->patientFacade->getAll();
    }
    
    public function renderEdit($id) {
        $patient = $this->patientFacade->get($id);
        $this->redirectIfRemoved($patient);
        
        $this["editPatientForm"]->setDefaults($patient->toArray());
    }
    
    public function renderDetail($id) {
        $this->template->patient = $this->patientFacade->get($id);
        $this->redirectIfRemoved($this->template->patient);
    }
    
    public function actionEdit($id) {
        $patient = $this->patientFacade->get($id);
        $this->redirectIfRemoved($patient);
    }
    
    public function actionDetail($id) {
        $patient = $this->patientFacade->get($id);
        $this->redirectIfRemoved($patient);
    }
    
    public function actionRemove($id, $redirectToDestination = null, $redirectArgs = array()) {
        $patient = $this->patientFacade->get($id);
        
        $this->patientFacade->remove($patient);
        
        $this->flashMessage("Pacient $patient->name úspěšně odstraněn.", "success");
        $redirectToDestination = ($redirectToDestination)? $redirectToDestination: "default";
        $this->redirect($redirectToDestination, $redirectArgs);
    }
    
    private function generatePatientForm() {
        $form = new Form();
        
        $allowed = "a-zA-ZěščřžýáíéďťňúůĚŠČŘŽÝÁÍÉĎŤŇÚŮ";
        
        $form->addText("name", "Jméno a příjmení")
                ->setRequired()
                ->addRule(Form::PATTERN, 'Vyplňte prosím jméno a příjmení', "[$allowed]{2,}( [$allowed]{2,})+");
        
         $form->addSelect("sex", "Pohlaví", Patient::getSexes())
                ->setRequired()
                ->setPrompt("vyberte pohlaví");
        
        $form->addText("givenId", "Rodné číslo")
                ->setRequired()
                ->addRule(Form::PATTERN, "Zadejte %label ve správném tvaru s lomítkem.", "[0-9]{6}/[0-9]{3,4}");
        
        $form->addDatePicker("dateOfBirth", "Datum narození")
                ->setRequired();
        
        $form->addClassSelect("insurance", "Pojišťovna", $this->insuranceFacade->getAllCurrent(), $this->insuranceFacade)
                ->setRequired()
                ->setPrompt("vyberte pojišťovnu");
        
        $form->addText("street", "Ulice")
                ->setRequired()
                ->addRule(Form::MAX_LENGTH, 'Jméno ulice je příliš dlouhé', 50);
        
        $form->addText("city", "Město")
                ->addRule(Form::MAX_LENGTH, 'Jméno města je příliš dlouhé', 50)
                ->setRequired();
        
        $form->addText("code", "PSČ")
                ->setRequired()
                ->addRule(Form::PATTERN, "Zadejte %label ve správném tvaru (pět čísel bez mezer).", "[0-9]{5}");
        
        $form->addText("phone", "Telefon")
                ->addRule(Form::PATTERN, "Zadejte %label ve tvaru AAABBBCCC, např. 728123456", "[0-9]{9}");
        
        $form->addEmail("email", "E-mail")
            ->setRequired();
        
        $form->onValidate[] = $this->validatePatientForm;
        
        return $form;
    }
    
    public function validatePatientForm(Form $form) {
        $this->patientFacade->validateProperty($form, "givenId", "Pacient s tímto rodným číslem je již v systému.");
        $this->patientFacade->validateProperty($form, "phone", "Toto telefonní číslo je již přiřazeno jinému pacientovi.");
        $this->patientFacade->validateProperty($form, "email", "Tento e-mail je již přiřazen jinému pacientovi."); 
    }
    
    public function createComponentAddPatientForm() {
        $form = $this->generatePatientForm();
        $form->addSubmit("ok", "Uložit");
        $form->addSubmit("cancel", "Zrušit")
                ->setAttribute('onclick', 'DefaultRedirect()');
        
        $form->onSuccess[] = $this->submittedAddPatientForm;
        
        return $form;
    }
    
    public function submittedAddPatientForm(Form $form) {
        $values = $form->getValues();
        
        $patient = new Patient();
        $patient->setValues($values);
        
        $this->patientFacade->create($patient);
        
        $this->flashMessage("Pacient $patient->name úspěšně pridán.", "success");
        $this->redirect("Patient:detail", $patient->id);
    }
    
    public function createComponentEditPatientForm() {
        $form = $this->generatePatientForm();
        
        $form->addHidden("id");
        $form->addSubmit("ok", "Uložit");
        $form->addSubmit("cancel", "Zrušit")
                ->setAttribute('onclick', 'DefaultRedirect()');
        
        $form->onSuccess[] = $this->submittedEditPatientForm;
        
        return $form;
    }
    
    public function submittedEditPatientForm(Form $form) {
        
        $values = $form->getValues();
        
        $patient = $this->patientFacade->get($values->id);
        
        $patient->setValues($values);
        
        $this->patientFacade->update($patient);
        
        $this->flashMessage("Základní informace úspěšně upraveny.", "success");
        $this->redirect("Patient:detail", $patient->id);
    }
}
