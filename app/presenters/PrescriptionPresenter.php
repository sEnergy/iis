<?php

use jb\Form\Form, 
 Facade\BaseFacade,
 Entity\Prescription;

class PrescriptionPresenter extends DoctorPresenter {
    
    /** @var BaseFacade */
    protected $prescriptionFacade;
    
    /** @var BaseFacade */
    protected $visitFacade;
    
    protected $visit;
    
    public function actionAdd($visitId) {
        $this->visit = $this->visitFacade->get($visitId);
        $this->template->visit = $this->visit;
    }
    
    public function createComponentAddPrescriptionForm() {
        $form = new Form();
        
        $this->generatePrescriptionFormBody($form);
        
        $form->addSubmit("ok", "Uložit");
        
        $form->onSuccess[] = $this->submittedAddPrescriptionForm;
        
        return $form;
    }
    
    public function submittedAddPrescriptionForm(Form $form) {
        $values = $form->getValues();
        
        $prescription = new Prescription();
        $prescription->setValues($values);
        $prescription->visit = $this->visit;
        
        $this->prescriptionFacade->create($prescription);
        
        $this->flashMessage("Předpis na lék $values->drug uložen.", "success");
        $this->redirect("Visit:detail", $this->visit->id);
    }
     
    public function renderEdit($id) {
        $prescription = $this->prescriptionFacade->get($id);
        $this->redirectIfRemoved($prescription);
        
        $this->template->prescription = $prescription;
        
        $this["editPrescriptionForm"]->setDefaults($prescription->toArray());
    }
    
    public function createComponentEditPrescriptionForm() {
        $form = new Form();
        
        $this->generatePrescriptionFormBody($form);
        
        $form->addHidden("id");
              
        $form->addSubmit("ok", "Uložit");
        
        $form->onSuccess[] = $this->submittedEditPrescriptionForm;
        
        return $form;
    }
    
    public function submittedEditPrescriptionForm(Form $form) { 
        $values = $form->getValues();
        $prescription = $this->prescriptionFacade->get($values->id);
        
        $prescription->setValues($values);
        $this->prescriptionFacade->update($prescription);
        
        $this->flashMessage("Předpis úspěšně změněn.", "success");
        $this->redirect("Visit:detail", $prescription->visit->id);
    }
    
    private function generatePrescriptionFormBody($form) {       
        $form->addText("drug", "Jméno léku")
                ->setRequired()
                ->addRule(Form::MAX_LENGTH, 'Jméno léku je příliš dlouhé', 50);
        
        $form->addTextArea("note", "Poznámka (dávkování atp.)")
                ->setRequired()
                ->setAttribute('rows', 4)
                ->addRule(Form::MAX_LENGTH, 'Poznámka je příliš dlouhá', 200);
    }
    
    public function actionRemove($id, $redirectToDestination = null, $redirectArgs = array()) {
        $prescription = $this->prescriptionFacade->get($id);
        $this->redirectIfRemoved($prescription);
     
        $this->prescriptionFacade->remove($prescription);
        
        $this->flashMessage("Předpis smazán.", "success");
        $redirectToDestination = ($redirectToDestination)? $redirectToDestination: "default";
        $this->redirect($redirectToDestination, $redirectArgs);
    } 
} 
