<?php

use jb\Form\Form;

class SignPresenter extends BasePresenter {
    
    public function beforeRender() {
        parent::beforeRender();

        $this->setLayout("notLoggedInLayout");
    }

    protected function createComponentSignInForm() {
        $form = new Form;
        
        $form->addClass("form-signin");
        $form->addText('username', 'Uživatel')
                ->setRequired();
        $form->addPassword('password', 'Heslo')
                ->setRequired();
        $form->addSubmit('ok', 'Přihlásit');

        $form->onSuccess[] = $this->signInFormSucceeded;
        
        return $form;
    }

    public function signInFormSucceeded($form) {
        $values = $form->getValues();

        try {
            $this->getUser()->login($values->username, $values->password);
            $this->redirect('Homepage:');
        } catch (Nette\Security\AuthenticationException $e) {
            $form->addError($e->getMessage());
        }
    }
    
}
