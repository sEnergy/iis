<?php

use jb\Form\Form, 
 Facade\BaseFacade,
 Entity\Order;

class OrderPresenter extends NursePresenter {
    
    /** @var BaseFacade */
    protected $orderFacade;
    
    /** @var BaseFacade */
    protected $patientFacade;
    
    /** @var BaseFacade */
    protected $visitFacade;
    
    /** @var BaseFacade */
    protected $treatmentFacade;
    
    public function renderDefault() {
        $this->template->orders = $this->orderFacade->getAll();
    }
    
    public function renderExpiring() {
    $expiring = array();
        
        $currentDate = new DateTime();
        $visits = $this->visitFacade->getAll();
        
        foreach ($visits as $visit){
            
            foreach ($visit->treatments as $treatment) {
                
                if ($treatment->expirationInMonths == NULL) {
                    continue;
                }
                
                $patientOrders = $this->orderFacade->getByColumn("patient", $visit->patient);
                
                foreach ($patientOrders as $order) {
                    if ($order->treatments->contains($treatment)) {
                        goto continue_jump;
                    }
                }
            
                $expiration = date_modify($visit->dateTime, '+'. $treatment->expirationInMonths . ' months');

                if (date_modify($currentDate, '+ 1 month') > $expiration){
                  $expiring[] = array($visit->patient, $treatment);
                }
                
                continue_jump:
            }
        }
        
        $this->template->expiring = $expiring;
    }
    
    public function renderDetail($id) {
        $this->template->order = $this->orderFacade->get($id);
        
        $this["addTreatmentForm"]->setDefaults($this->template->order->toArray());
    }
    
    public function actionAdd($id = NULL) {
        
        $this->template->patient = NULL;
        
        if ($id !== NULL) {
           $this->template->patient = $this->patientFacade->get($id);
           
           if (!$this->template->patient){
               $this->redirect("default");
           }
           else {
               $this["addOrderForm"]->setDefaults(array("patient" => $this->template->patient));
           }   
        } 
    }
    
    public function createComponentAddOrderForm() {
        $form = new Form();
        
        $this->generateOrderFormBody($form);
        
        $form->addSubmit("ok", "Uložit");
        
        $form->onSuccess[] = $this->submittedAddOrderForm;
        
        return $form;
    }
    
    public function submittedAddOrderForm(Form $form) {
        $values = $form->getValues();
        
        $order = new Order();
        $order->setValues($values);
        
        $this->orderFacade->create($order);
        $this->orderFacade->updateNow();
        
        $newest = $this->orderFacade->getOneByCriteria(array("dateTime"=> $order->dateTime, "patient" => $order->patient));
        
        $this->flashMessage("Návštěva naplánována", "success");
        $this->redirect("Order:detail", $newest->id);
    }
     
    public function renderEdit($id) {
        $order = $this->orderFacade->get($id);
        $this->redirectIfRemoved($order);
        
        $this->template->order = $order;
        
        $this["editOrderForm"]->setDefaults($order->toArray());
    }
    
    public function createComponentEditOrderForm() {
        $form = new Form();
        
        $this->generateOrderFormBody($form);
        
        $form->addHidden("id");
              
        $form->addSubmit("ok", "Uložit");
        
        $form->onSuccess[] = $this->submittedEditOrderForm;
        
        return $form;
    }
    
    public function submittedEditOrderForm(Form $form) { 
        $values = $form->getValues();
        $order = $this->orderFacade->get($values->id);
        
        $order->setValues($values);
        $this->orderFacade->update($order);
        
        $this->flashMessage("Objednaná návštěva upravena.", "success");
        $this->redirect("Order:default");
    }
    
    private function generateOrderFormBody($form) { 
        
        $form->addClassSelect("patient", "Pacient", $this->patientFacade->getAll(), $this->patientFacade)
                ->setRequired()
                ->setPrompt("vyberte pacienta");
        
        $form->addDateTimePicker("dateTime", "Den a čas")
                ->setRequired();
    }
    
    public function validateTreatmentFormBody(Form $form) {
        
        $values = $form->getValues();
        $order = $this->orderFacade->get($values->id);
        
        if ($order !== NULL) {
                if (in_array ( $values->treatment , $order->treatments->toArray())) {
                $form["treatment"]->addError("Tento zákrok je již k návštěvě přiřazen.");
            }
        }
    }
    
    public function actionRemove($id, $redirectToDestination = null, $redirectArgs = array()) {
        $order = $this->orderFacade->get($id);
        
        $this->redirectIfRemoved($order);
        
        $patient = $order->patient;
        
        $this->orderFacade->remove($order);
        
        //$patient->orders->removeElement($order);
        //$this->patientFacade->update($patient);
        
        $this->flashMessage("Objednaná návštěva smazána.", "success");
        $redirectToDestination = ($redirectToDestination)? $redirectToDestination: "default";
        $this->redirect($redirectToDestination, $redirectArgs);
    } 
    
    public function createComponentAddTreatmentForm() {
        $form = new Form();
        
        $form->addHidden("id");
        
        $form->addClassSelect("treatment", "", $this->treatmentFacade->getAllCurrent(), $this->treatmentFacade)
                ->setRequired()
                ->setPrompt("vyberte zákrok, který chcete přidat");
        
        $form->addSubmit("ok", "Přidat");
        
        $form->onValidate[] = $this->validateTreatmentFormBody;
        $form->onSuccess[] = $this->submittedAddTreatmentForm;
        
        return $form;
    }
    
    public function submittedAddTreatmentForm(Form $form) {
        
        $values = $form->getValues();
        $order = $this->orderFacade->get($values->id);
        
        $order->treatments[] = $values->treatment;
        $this->orderFacade->update($order);
        
        $this->flashMessage("Zákrok přidán.", "success");
        $this->redirect("Order:detail", $order->id);
    }
    
    public function actionRemoveTreatment($orderId, $treatmentId, $redirectToDestination = null, $redirectArgs = array()) {
        $order = $this->orderFacade->get($orderId);
        $this->redirectIfRemoved($order);
        
        foreach ($order->treatments as $treatment) {
            if ($treatment->id == $treatmentId) {
                $order->treatments->removeElement($treatment);
                $this->orderFacade->update($order);
                
                $this->flashMessage("Výkon úspěšně odstraněn.", "success");
                $redirectToDestination = ($redirectToDestination)? $redirectToDestination: "default";
                $this->redirect($redirectToDestination, $redirectArgs);
            }
        }
        
        $redirectToDestination = ($redirectToDestination)? $redirectToDestination: "default";
        $this->redirect($redirectToDestination, $redirectArgs);
    }
} 
