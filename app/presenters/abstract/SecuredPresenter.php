<?php

use jb\Model\Entities\BaseEntity,
    Facade\BaseFacade,
    jb\Form\Form;

abstract class SecuredPresenter extends BasePresenter {
   
    /** @var BaseEntity */
    protected $employeeEntity;
    
    /** @var BaseEntity */
    private $employeeFacade;
    
    public function __construct(\Nette\DI\Container $context = NULL) {
        parent::__construct($context);
        $this->employeeFacade = $context->getService("employeeFacade");
    }
    
    public function startup() {
        parent::startup();
        
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect("Sign:");
        }
        
        $this->employeeEntity = $this->employeeFacade->get($this->getUser()->getId());
        
        $this->template->userRole = $this->employeeEntity->role;
        
        $this->verifyAccess();
        
        //$params = $this->context->getParameters();
    }
    
    public function actionLogout() {
        $this->getUser()->logout(true);
        $this->flashMessage('Byl(a) jste úspěšně odhlášen(a).');
        $this->redirect('Sign:');
    }
    
    protected function calculateHash($password) {
        return $this->context->getService("authenticator")->calculateHash($password);
    }
    
    protected function verifyAccess() {
        
    }
}
