<?php

abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    public function __construct(\Nette\DI\Container $context = NULL) {
        parent::__construct($context); 
        
        $this->init($context);
        
    }
    
    private function init(\Nette\DI\Container $context = null) {
        foreach ($this->getReflection()->getProperties() as $property) {
            if ($property->protected && 
                    (Nette\Utils\Strings::endsWith($property->name, "Facade") ||
                    Nette\Utils\Strings::endsWith($property->name, "Service"))) {
                $name = $property->name;
                $this->$name = $context->$name;
            }
        }
    }
    
    public function startup() {
        parent::startup();
        
        $redirect = $this->getHttpRequest()->getQuery("redirect");
        if ($redirect !== null) {
            
            $session = $this->getSession("redirect");
            if (is_array($redirect)) {
                $redirect = (object) $redirect;
                $session->dest = $redirect->dest;
                $session->args = is_array($redirect->args)? $redirect->args: array($redirect->args);
            }
            else {
                $session->dest = $redirect;
            }
            
        }
    }
    
    public function redirectIfRemoved($entity) {
        if (!$entity || (property_exists($entity, "removed") && $entity->removed)) {
            $this->redirect("default");
        }
    }
    
}
