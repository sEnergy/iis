<?php

use jb\Model\Entities\BaseEntity,
    Facade\BaseFacade,
    jb\Form\Form;

abstract class DoctorPresenter extends SecuredPresenter {
    
    protected function verifyAccess() {
        if ($this->employeeEntity->role == "nurse") {
                $this->redirect("Homepage:");
        }
    }
    
}
