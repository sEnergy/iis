<?php

use jb\Model\Entities\BaseEntity,
    Facade\BaseFacade,
    jb\Form\Form;

abstract class AdminPresenter extends SecuredPresenter {
    
    protected function verifyAccess() {
        if ($this->employeeEntity->role != "admin") {
                $this->redirect("Homepage:");
        }
    }
    
}
