<?php

use jb\Form\Form, 
 Facade\BaseFacade,
 Entity\Extracheckup;

class ExtracheckupPresenter extends DoctorPresenter {
    
    /** @var BaseFacade */
    protected $extracheckupFacade;
    
    /** @var BaseFacade */
    protected $visitFacade;
    
    protected $visit;
    
    public function actionAdd($visitId) {
        $this->visit = $this->visitFacade->get($visitId);
        $this->template->visit = $this->visit;
    }
    
    public function createComponentAddExtracheckupForm() {
        $form = new Form();
        
        $this->generateExtracheckupFormBody($form);
        
        $form->addSubmit("ok", "Uložit");
        
        $form->onSuccess[] = $this->submittedAddExtracheckupForm;
        
        return $form;
    }
    
    public function submittedAddExtracheckupForm(Form $form) {
        $values = $form->getValues();
        
        $extracheckup = new Extracheckup();
        $extracheckup->setValues($values);
        $extracheckup->visit = $this->visit;
        
        $this->extracheckupFacade->create($extracheckup);
        
        $this->flashMessage("Záznam o doplňující prohlídce uložen.", "success");
        $this->redirect("Visit:detail", $this->visit->id);
    }
     
    public function renderEdit($id) {
        $extracheckup = $this->extracheckupFacade->get($id);
        $this->redirectIfRemoved($extracheckup);
        
        $this->template->extracheckup = $extracheckup;
        
        $this["editExtracheckupForm"]->setDefaults($extracheckup->toArray());
    }
    
    public function createComponentEditExtracheckupForm() {
        $form = new Form();
        
        $this->generateExtracheckupFormBody($form);
        
        $form->addHidden("id");
              
        $form->addSubmit("ok", "Uložit");
        
        $form->onSuccess[] = $this->submittedEditExtracheckupForm;
        
        return $form;
    }
    
    public function submittedEditExtracheckupForm(Form $form) { 
        $values = $form->getValues();
        $extracheckup = $this->extracheckupFacade->get($values->id);
        
        $extracheckup->setValues($values);
        $this->extracheckupFacade->update($extracheckup);
        
        $this->flashMessage("Záznam o doplňující prohlídce úspěšně změněn.", "success");
        $this->redirect("Visit:detail", $extracheckup->visit->id);
    }
    
    private function generateExtracheckupFormBody($form) {       
        $form->addSelect("clinic", "Oddělení", Extracheckup::getClinics())
                ->setRequired()
                ->setPrompt("vyberte oddělení");
        
        $form->addTextArea("subject", "Důvod vyšetření")
                ->setRequired()
                ->setAttribute('rows', 2)
                ->addRule(Form::MAX_LENGTH, 'Důvod vyšetření je příliš dlouhý', 100);
        
        $form->addTextArea("result", "Výsledek")
                ->setRequired()
                ->setAttribute('rows', 7)
                ->addRule(Form::MAX_LENGTH, 'Popis výsledku vyšetření může mít maximální délku %d znaků.', 500);
    }
    
    public function actionRemove($id, $redirectToDestination = null, $redirectArgs = array()) {
        $extracheckup = $this->extracheckupFacade->get($id);
        $this->redirectIfRemoved($extracheckup);
     
        $this->extracheckupFacade->remove($extracheckup);
        
        $this->flashMessage("Záznam o doplňující vyšetření smazán.", "success");
        $redirectToDestination = ($redirectToDestination)? $redirectToDestination: "default";
        $this->redirect($redirectToDestination, $redirectArgs);
    } 
} 
