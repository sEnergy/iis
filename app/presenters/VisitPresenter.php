<?php

use jb\Form\Form, 
 Facade\BaseFacade,
 Entity\Visit;

class VisitPresenter extends DoctorPresenter {
    
    /** @var BaseFacade */
    protected $visitFacade;
    
    /** @var BaseFacade */
    protected $patientFacade;
    
    /** @var BaseFacade */
    protected $treatmentFacade;
    
    /** @var BaseFacade */
    protected $insuranceFacade;
    
    protected $patient;
    
    public function renderUncovered() {
        $this->template->visits = $this->visitFacade->getByColumn("covered", false);
        
        $this->template->uniquePatients = $this->getSortedUniqueContents('patient');
        $this->template->uniqueInsurance = $this->getSortedUniqueContents('insurance', 'shortName');
    }
    
    protected function getSortedUniqueContents ($facadeName, $property = NULL) {
        $recordlist = $this->{$facadeName.'Facade'}->getAll(); 
        $opitonList = array();
        
        if ($property == NULL) {
            $property = 'name';
        }

        foreach($recordlist as $record) {
             $opitonList[] = $record->{$property};
        }

        return $this->uniqueSortArray($opitonList); 
    }
    
    private function uniqueSortArray (array $arr) {
        $uniqueArr = array_unique($arr);
        asort($uniqueArr);
        return $uniqueArr;
    }
    
    public function renderCovered() {
        $this->template->visits = $this->visitFacade->getByColumn("covered", true);
    }
    
    public function renderDetail($id) {
        $this->template->visit = $this->visitFacade->get($id);
        $this->redirectIfRemoved($this->template->visit);
        
        $this["addTreatmentForm"]->setDefaults($this->template->visit->toArray());
    }
    
    public function actionAdd($patientId) {
        $this->patient = $this->patientFacade->get($patientId);
        $this->template->patientName = $this->patient->name;
        $this->template->patientId = $this->patient->id;
    }
    
    public function createComponentAddVisitForm() {
        $form = new Form();
        
        $this->generateVisitFormBody($form);
        
        $form->addSubmit("ok", "Uložit");
        
        $form->onSuccess[] = $this->submittedAddVisitForm;
        
        return $form;
    }
    
    public function submittedAddVisitForm(Form $form) {
        $values = $form->getValues();
        
        $visit = new Visit();
        $visit->setValues($values);
        $visit->patient = $this->patient;
        
        $this->visitFacade->create($visit);
        $this->visitFacade->updateNow();
        
        $newest = $this->visitFacade->getOneByColumn("dateTime", $visit->dateTime);
        
        $this->flashMessage("Návštěva zaznamenána.", "success");
        $this->redirect("Visit:detail", $newest->id);
    }
     
    public function renderEdit($id) {
        $visit = $this->visitFacade->get($id);
        $this->redirectIfRemoved($visit);
        
        $this["editVisitForm"]->setDefaults($visit->toArray());
        
        $this->template->visitId = $visit->id;
    }
    
    public function createComponentEditVisitForm() {
        $form = new Form();
        
        $this->generateVisitFormBody($form);
        
        $form->addHidden("id");
              
        $form->addSubmit("ok", "Uložit");
        
        $form->onSuccess[] = $this->submittedEditVisitForm;
        
        return $form;
    }
    
    public function createComponentAddTreatmentForm() {
        $form = new Form();
        
        $form->addHidden("id");
        
        $form->addClassSelect("treatment", "", $this->treatmentFacade->getAllCurrent(), $this->treatmentFacade)
                ->setRequired()
                ->setPrompt("vyberte zákrok, který chcete přidat");
        
        $form->addSubmit("ok", "Přidat");
        
        $form->onValidate[] = $this->validateTreatmentFormBody;
        $form->onSuccess[] = $this->submittedAddTreatmentForm;
        
        return $form;
    }
    
    public function submittedAddTreatmentForm(Form $form) {
        
        $values = $form->getValues();
        $visit = $this->visitFacade->get($values->id);
        
        $visit->treatments[] = $values->treatment;
        $this->visitFacade->update($visit);
        
        $this->flashMessage("Zákrok přidán.", "success");
        $this->redirect("Visit:detail", $values->id);
    }
    
    public function validateTreatmentFormBody(Form $form) {
        
        $values = $form->getValues();
        $visit = $this->visitFacade->get($values->id);
        
        if (in_array ( $values->treatment , $visit->treatments->toArray())) {
            $form["treatment"]->addError("Tento zákrok je již k návštěvě přiřazen.");
        }
    }
    
    public function submittedEditVisitForm(Form $form) {
        
        $values = $form->getValues();
        $visit = $this->visitFacade->get($values->id);
        
        $visit->setValues($values);
        $this->visitFacade->update($visit);
        
        $this->flashMessage("Záznam o návštěvě úspěšně změněn.", "success");
        $this->redirect("Visit:detail", $values->id);
    }
    
    private function generateVisitFormBody($form) {      
        $form->addTextArea("detail", "Detail")
                ->setAttribute('rows', 7)
                ->setRequired()
                ->addRule(Form::MAX_LENGTH, 'Detail návštěvy je příliš dlouhý', 500);
        
        $form->addText("price", "K proplacení pojišťovnou")
                ->addRule(Form::PATTERN, "Zadejte prosím částku v celých korunách.","[0-9]{0,10}");
    }
    
    public function actionCover($id, $redirectToDestination = null, $redirectArgs = array()) {
        $visit = $this->visitFacade->get($id);
        $this->redirectIfRemoved($visit);
     
        $visit->covered = true;
        $this->visitFacade->update($visit);
        
        $this->flashMessage("Položka označena jako proplacená.", "success");
        $redirectToDestination = ($redirectToDestination)? $redirectToDestination: "default";
        $this->redirect($redirectToDestination, $redirectArgs);
    } 
    
    public function actionRemove($id, $redirectToDestination = null, $redirectArgs = array()) {
        $visit = $this->visitFacade->get($id);
        $this->redirectIfRemoved($visit);
     
        $this->visitFacade->remove($visit);
        
        $this->flashMessage("Návštěva odstraněna.", "success");
        $redirectToDestination = ($redirectToDestination)? $redirectToDestination: "default";
        $this->redirect($redirectToDestination, $redirectArgs);
    }
    
    public function actionRemoveTreatment($visitId, $treatmentId, $redirectToDestination = null, $redirectArgs = array()) {
        $visit = $this->visitFacade->get($visitId);
        $this->redirectIfRemoved($visit);
        
        foreach ($visit->treatments as $treatment) {
            if ($treatment->id == $treatmentId) {
                $visit->treatments->removeElement($treatment);
                $this->visitFacade->update($visit);
                
                $this->flashMessage("Výkon úspěšně odstraněn.", "success");
                $redirectToDestination = ($redirectToDestination)? $redirectToDestination: "default";
                $this->redirect($redirectToDestination, $redirectArgs);
            }
        }
        
        $redirectToDestination = ($redirectToDestination)? $redirectToDestination: "default";
        $this->redirect($redirectToDestination, $redirectArgs);
    }
} 
