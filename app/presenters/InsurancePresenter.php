<?php

use jb\Form\Form, 
 Facade\BaseFacade,
 Entity\Insurance;

class InsurancePresenter extends DoctorPresenter {
    
    /** @var BaseFacade */
    protected $insuranceFacade;
    
    public function renderDefault() {
        $this->template->insurances = $this->insuranceFacade->getAllCurrent();
    }
    
    public function renderArchive() {
        $this->template->insurances = $this->insuranceFacade->getAllArchived();
    }
    
    public function renderEdit($id) {
        $insurance = $this->insuranceFacade->get($id);
        $this->redirectIfRemoved($insurance);
        
        $this["editInsuranceForm"]->setDefaults($insurance->toArray());
    }
 
    public function actionEdit($id) {
       $insurance = $this->insuranceFacade->get($id);
       $this->redirectIfRemoved($insurance);
    }
    
    public function createComponentAddInsuranceForm() {
        $form = new Form();
        $this->generateInsuranceFormBody($form);
        
        $form->addSubmit("ok", "Uložit");
        $form->addButton('cancel', 'Zrušit')
                ->setAttribute('onclick', 'DefaultRedirect()');
        
        $form->onSuccess[] = $this->submittedAddInsuranceForm;
        
        return $form;
    }
    
    public function createComponentEditInsuranceForm() {
        $form = new Form();
        
        $this->generateInsuranceFormBody($form);
        
        $form->addHidden("id");
        
        $form->addSubmit("ok", "Uložit");
        $form->addButton('raise', 'Zrušit')
                ->setAttribute('onclick', 'DefaultRedirect()');
        
        $form->onSuccess[] = $this->submittedEditInsuranceForm;
        
        return $form;
    }
    
    private function generateInsuranceFormBody($form) {
        $form->addText("name", "Jméno pojišťovny")
                ->addRule(Form::MAX_LENGTH, 'Jméno je příliš dlouhé', 50)
                ->setRequired();
        
        $form->addText("shortName", "Zkratka jména")
                ->addRule(Form::MAX_LENGTH, 'Zkratka je příliš dlouhá', 6)
                ->setRequired();

        $form->onValidate[] = $this->validateInsuranceFormBody;
    }
    
    public function validateInsuranceFormBody(Form $form) {
    
        $errorMessageEnd = " se v systému již nachází. Je možné, že se jedná o pojišťovnu, se kterou nyní nemáme smlouvu (viz. Pojišťovny bez smlouvy).";
        
        $this->insuranceFacade->validateProperty($form, "name", "Zadané jméno pojišťovny" . $errorMessageEnd);
        $this->insuranceFacade->validateProperty($form, "shortName", "Zadaná zkratka pojišťovny" . $errorMessageEnd);
    }
    
    public function submittedAddInsuranceForm(Form $form) {
        $values = $form->getValues();
        
        $insurance = new Insurance();
        $insurance->setValues($values);
        
        $this->insuranceFacade->create($insurance);
        
        $this->flashMessage("Pojišťovna $insurance->name úspěšně přidána do systému.", "success");
        $this->redirect("default");
    }
    
    public function submittedEditInsuranceForm(Form $form) {
        
        $values = $form->getValues();
        $insurance = $this->insuranceFacade->get($values->id);
        
        $insurance->setValues($values);
        $this->insuranceFacade->update($insurance);
        
        $this->flashMessage("Pojišťovna $insurance->name úspěšně upravena.", "success");
        $this->redirect("default");
    }
    
    public function actionRemove($id, $redirectToDestination = null, $redirectArgs = array()) {
        $insurance = $this->insuranceFacade->get($id);
        $this->redirectIfRemoved($insurance);
     
        $this->insuranceFacade->remove($insurance);
        
        $this->flashMessage("S pojišťovnou $insurance->name byla rozvázána smlouva a byla přesunuta do Pojišťoven bez smlouvy.", "success");
        $redirectToDestination = ($redirectToDestination)? $redirectToDestination: "default";
        $this->redirect($redirectToDestination, $redirectArgs);
    }
    
    public function actionRestore($id, $redirectToDestination = null, $redirectArgs = array()) {
        $insurance = $this->insuranceFacade->get($id);
        
        $this->insuranceFacade->restore($insurance);
        
        $this->flashMessage("S pojišťovnou $insurance->name úspěšně uzavřena nová smlouva.", "success");
        $redirectToDestination = ($redirectToDestination)? $redirectToDestination: "default";
        $this->redirect($redirectToDestination, $redirectArgs);
    } 
} 
