<?php

use Tracy\Debugger;

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

Debugger::enable(Debugger::PRODUCTION);

$configurator->setDebugMode(FALSE); 
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

Nette\Diagnostics\Debugger::$editor = 'netbeans://open/?url=file://%file&line=%line';

$configurator->enableDebugger(__DIR__ . '/../log');
$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->addDirectory(__DIR__ . '/../vendor/others')
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');

$container = $configurator->createContainer();

return $container;
