<?php

namespace Entity;

use jb\Model\Entities\BaseEntity;
use Doctrine\ORM\Mapping as ORM,
    \Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="extracheckup") 
 */
class Extracheckup extends BaseEntity {
    
    const 
            CLINIC_EYE = "eye",
            CLINIC_GYNO = "gyno",
            CLINIC_CANCER = "cancer",
            CLINIC_ORTOPEDICS = "ortopedics",
            CLINIC_PSYCHO = "psycho",
            CLINIC_NEURO = "neuro",
            CLINIC_INFECTIONS = "infections",
            CLINIC_SKIN = "skin",
            CLINIC_UROLOGY = "urology";
    
    private static $clinics = array(
        self::CLINIC_EYE => "Oční",
        self::CLINIC_GYNO => "Gynekologie",
        self::CLINIC_CANCER => "Onkologie",
        self::CLINIC_ORTOPEDICS => "Ortopedie",
        self::CLINIC_PSYCHO => "Psychiatrie",
        self::CLINIC_NEURO => "Neurologie",
        self::CLINIC_INFECTIONS => "Infekční",
        self::CLINIC_SKIN => "Kožní",
        self::CLINIC_UROLOGY => "Urologie"
    );
    
    /**
     * @ORM\Column(length=10, nullable=false)
     * @var string 
     */
    protected $clinic;
    
    /**
     * @ORM\Column(length=100, nullable=false)
     * @var string 
     */
    protected $subject;
    
    /**
     * @ORM\Column(length=500, nullable=false)
     * @var string 
     */
    protected $result;
    
    /**
     * @ORM\ManyToOne(targetEntity="Visit", inversedBy="extracheckups")
     * @var Visit 
     */
    protected $visit;
    
    public static function getClinics() {
        asort(self::$clinics);
        return self::$clinics;
    }
    
    public function getNiceClinic() {
        return self::$clinics[$this->clinic];
    }
    
}
