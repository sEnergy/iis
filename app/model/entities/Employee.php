<?php

namespace Entity;

use jb\Model\Entities\BaseEntity;
use Doctrine\ORM\Mapping as ORM,
    \Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="employee")
 */
class Employee extends BaseEntity {
    
    const 
            ROLE_ADMIN = "admin",
            ROLE_DOCTOR = "doctor",
            ROLE_NURSE = "nurse";
    
    private static $roles = array(
        self::ROLE_ADMIN => "správce systému",
        self::ROLE_DOCTOR => "doktor",
        self::ROLE_NURSE => "sestra"
    );
    
    /**
     * @ORM\Column(unique=true, length=50, nullable=false)
     * @var string 
     */
    protected $username;
    
    /**
     * @ORM\Column(length=20, nullable=false)
     * @var string 
     */
    protected $password;
    
    /**
     * @ORM\Column(length=50, nullable=false)
     * @var string 
     */
    protected $name;
    
    /**
     * @ORM\Column(length=6, nullable=false)
     * @var string 
     */
    protected $role;
    
    /**
     * @ORM\Column(unique=true, length=9, nullable=false)
     * @var string 
     */
    protected $phone;
    
    public static function getRoles() {
        return self::$roles;
    }
    
    public function getNiceRole() {
        return self::$roles[$this->role];
    }
    
}
