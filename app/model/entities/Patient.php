<?php

namespace Entity;

use jb\Model\Entities\BaseEntity;
use Doctrine\ORM\Mapping as ORM,
    \Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="patient") 
 */
class Patient extends BaseEntity {
    
    const SEX_MALE = "male",
          SEX_FEMALE = "female";
    
    private static $sexes = array(
        self::SEX_MALE => "muž",
        self::SEX_FEMALE => "žena"
    );
    
    /**
     * @ORM\Column(unique=true, name="givenId", length=15, nullable=false)
     * @var string 
     */
    protected $givenId;
    
    /**
     * @ORM\Column(length=50, nullable=false)
     * @var string 
     */
    protected $name;
    
    /**
     * @ORM\Column(length=6, nullable=false)
     * @var string 
     */
    protected $sex;
    
    /**
     * @ORM\Column(type="datetime", name="dateOfBirth", nullable=false)
     * @var \DateTime
     */
    protected $dateOfBirth;
    
    
    /**
     * @ORM\Column(length=50, nullable=false)
     * @var string 
     */
    protected $city;
    
    /**
     * @ORM\Column(length=50, nullable=false)
     * @var string 
     */
    protected $street;
    
    /**
     * @ORM\Column(length=5, nullable=false)
     * @var string
     */
    protected $code;
    
    /**
     * @ORM\Column(unique=true, length=9, nullable=false)
     * @var string 
     */
    protected $phone;
    
    /**
     * @ORM\Column(unique=true, length=50, nullable=false)
     * @var string 
     */
    protected $email;
    
    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=false)
     * @var \DateTime
     */
    protected $registerDate;
    
    /**
     * @ORM\ManyToOne(targetEntity="Insurance", inversedBy="patient")
     * @var Insurance 
     */
    protected $insurance;
    
    /**
     * @ORM\OneToMany(targetEntity="Visit", mappedBy="patient")
     * @var \ArrayCollection 
     */
    protected $visits;
    
    /**
     * @ORM\OneToMany(targetEntity="Order", mappedBy="patient")
     * @var \ArrayCollection 
     */
    protected $orders;
    
    public static function getSexes() {
        return self::$sexes;
    }
    
    public function getNiceSex() { // if you smile, you are dirty-minded as f*ck
        return self::$sexes[$this->sex];
    }
    
    public function __construct() {
        parent::__construct();
        
        $this->registerDate = new \DateTime();
        $this->visits = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }
    
    public function __toString() {
        return $this->name;
    }
    
}
