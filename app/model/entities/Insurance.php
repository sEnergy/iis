<?php

namespace Entity;

use jb\Model\Entities\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="insurance") 
 */
class Insurance extends BaseEntity {
    
    /**
     * @ORM\Column(length=50, nullable=false)
     * @var string 
     */
    protected $name;
    
    /**
     * @ORM\Column(length=6, name="shortName", nullable=false)
     * @var string 
     */
    protected $shortName;
    
    /**
     * @ORM\OneToMany(targetEntity="Patient", mappedBy="insurance")
     * @var \ArrayCollection 
     */
    protected $patients;
    
    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @var bool 
     */
    protected $removed;
    
    public function __toString() {
        return $this->name;
    }
    
    public function __construct() {
        parent::__construct();
        
        $this->removed = false;
        $this->patients = new ArrayCollection();
    }
    
    public function getRelations() {
        return $this->patients;
    }
}
