<?php

namespace Entity;

use jb\Model\Entities\BaseEntity;
use Doctrine\ORM\Mapping as ORM,
    \Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="treatment") 
 */
class Treatment extends BaseEntity {
    
    /**
     * @ORM\Column(length=50, nullable=false)
     * @var string 
     */
    protected $name;
    
    /**
     * @ORM\Column(type="integer", nullable=true, name="expirationInMonths")
     * @var integer
     */
    protected $expirationInMonths;
    
    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @var bool 
     */
    protected $removed;
    
    public function __construct() {
        parent::__construct();
        
        $this->removed = false;
    }
    
    public function getNiceExpiration () {
        
        if ($this->expirationInMonths >= 5) {
           return $this->expirationInMonths . " měsíců";
        }
        else if ($this->expirationInMonths >= 2) {
           return $this->expirationInMonths . " měsíce";
        }
        else if ($this->expirationInMonths) {
           return $this->expirationInMonths . " měsíc";
        }
        
        return "není potřeba";
    }
    
    public function __toString() {
        return $this->name;
    }
    
}
