<?php

namespace Entity;

use jb\Model\Entities\BaseEntity;
use Doctrine\ORM\Mapping as ORM,
    \Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="prescription") 
 */
class Prescription extends BaseEntity {
    
    /**
     * @ORM\Column(length=50, nullable=false)
     * @var string 
     */
    protected $drug;
    
    /**
     * @ORM\Column(length=200, nullable=false)
     * @var string 
     */
    protected $note;
    
    /**
     * @ORM\ManyToOne(targetEntity="Visit", inversedBy="prescriptions")
     * @var Visit 
     */
    protected $visit;
  
}
