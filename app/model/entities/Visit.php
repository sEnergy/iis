<?php

namespace Entity;

use jb\Model\Entities\BaseEntity;
use Doctrine\ORM\Mapping as ORM,
    \Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="visit") 
 */
class Visit extends BaseEntity {
    
    /**
     * @ORM\ManyToOne(targetEntity="Patient", inversedBy="visits")
     * @var Insurance 
     */
    protected $patient;
    
    /**
     * @ORM\Column(type="datetime", name="dateTime", nullable=false)
     * @var \DateTime
     */
    protected $dateTime;
    
    /**
     * @ORM\Column(length=500, nullable=false)
     * @var string 
     */
    protected $detail;
    
    /**
     * @ORM\Column(type="integer", nullable=false)
     * @var integer
     */
    protected $price;
    
    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @var bool 
     */
    protected $covered;
    
    /**
     * @ORM\OneToMany(targetEntity="Prescription", mappedBy="visit", orphanRemoval=true, cascade={"remove"})
     * @var \ArrayCollection 
     */
    protected $prescriptions;
    
    /**
     * @ORM\OneToMany(targetEntity="Extracheckup", mappedBy="visit", orphanRemoval=true, cascade={"remove"})
     * @var \ArrayCollection 
     */
    protected $extracheckups;
    
    /**
     * @ORM\ManyToMany(targetEntity="Treatment", cascade={"remove"})
     * @ORM\JoinTable(name="visit_treatment",
     *      joinColumns={@ORM\JoinColumn(name="visit_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="treatment_id", referencedColumnName="id")}
     *      )
     **/
    protected $treatments;
    
    public function __construct() {
        parent::__construct();
        
        $this->dateTime = new \DateTime();
        
        $this->covered = false;
        
        $this->prescriptions = new ArrayCollection();
        $this->extracheckups = new ArrayCollection();
        $this->treatments = new ArrayCollection();
    }
    
    public function getDetailSnippet () {
        $snippetLengthLimit = 100;
        return (strlen($this->detail) > $snippetLengthLimit) ? substr($this->detail,0,$snippetLengthLimit).'...' : $this->detail;
    }   
}
