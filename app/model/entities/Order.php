<?php

namespace Entity;

use jb\Model\Entities\BaseEntity;
use Doctrine\ORM\Mapping as ORM,
    \Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="visitorder") 
 */
class Order extends BaseEntity {
    
    /**
     * @ORM\Column(type="datetime", name="dateTime", nullable=false)
     * @var \DateTime
     */
    protected $dateTime;
    
    /**
     * @ORM\ManyToOne(targetEntity="Patient", inversedBy="orders")
     * @var Insurance 
     */
    protected $patient;
    
    /**
     * @ORM\ManyToMany(targetEntity="Treatment")
     * @ORM\JoinTable(name="order_treatment",
     *      joinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="treatment_id", referencedColumnName="id")}
     *      )
     **/
    protected $treatments;
    
    public function __construct() {
        parent::__construct();
        
        $this->treatments = new ArrayCollection();
    }
     
    public function getNiceTreatmentListing () {
        if ($this->treatments->count()){
            return implode(", ", $this->treatments->toArray());
        }
        else {
            return "zákroky nespeficikovány";
        } 
    }
}
