<?php

namespace Facade;

use jb\Model\Entities\BaseEntity;

class VisitFacade extends BaseFacade {  

    public function updateNow() {
       $this->dao->save();
    }
    
}
