<?php

namespace Facade;

class BaseFacade extends \jb\Model\Facades\BaseFacade {  
    
    public function validateProperty ($form, $propertyName, $errorMessage) {
        $values = $form->getValues();
        
        $editing = isset($values->id);
        
        if ($editing) {
            $result = $this->getOneByColumn($propertyName, $values->{$propertyName});
            if (($result !== null) && ($result->id != $values->id)) {
                $form[$propertyName]->addError($errorMessage);
            }
        }
        else {
            $result = $this->getOneByColumn($propertyName, $values->{$propertyName});
            if (($result !== null)) {
                $form[$propertyName]->addError($errorMessage);
            }
        }
    }
    
    public function getAllCurrent() {
        return $this->getByColumn("removed", false);
    }
    
    public function getAllArchived() {
        return $this->getByColumn("removed", true);
    }
    
    public function restore ($entity) {
        $entity->removed = false;
        $this->update($entity);
    }
}
