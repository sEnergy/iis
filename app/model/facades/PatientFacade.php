<?php

namespace Facade;

use jb\Model\Entities\BaseEntity;

class PatientFacade extends BaseFacade {  
    
    protected function doRemove(BaseEntity $entity) {
       
        
        // delete of visits
        $facade = $this->dao->related("visits");
        
        foreach ($entity->visits as $visit) {
                $facade->delete($visit, false);
                $entity->visits->removeElement($visit);
        }
        
        $facade->save();
        
        // delete of orders
        $facade = $this->dao->related("orders");
        
        foreach ($entity->orders as $order) {
                $facade->delete($order, false);
                $entity->orders->removeElement($order);
        }
        
        $facade->save();
        
        $this->dao->delete($entity, null);
    } 
    
}
