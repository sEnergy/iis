<?php

namespace Facade;

use jb\Model\Entities\BaseEntity;

class OrderFacade extends BaseFacade {  

    public function updateNow() {
       $this->dao->save();
    }
    
    protected function doRemove(BaseEntity $entity) {

        // delete of visits
        $facade = $this->dao->related("treatments");
        
        foreach ($entity->treatments as $treatment) {
                $entity->treatments->removeElement($treatment);
        }
        
        $facade->save();
        
        $this->dao->delete($entity, null);
    } 
    
}
