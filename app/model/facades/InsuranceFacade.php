<?php

namespace Facade;

use jb\Model\Entities\BaseEntity;

class InsuranceFacade extends BaseFacade {  
    
    protected function doRemove(BaseEntity $entity) {
        foreach ($entity->getRelations()as $patient) {
            $patient->insurance = null;
        }
        
        $this->dao->related("patients")->save();
        
        $entity->removed = true;
        $this->dao->save();
    }
    
}
