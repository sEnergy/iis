<?php

use Nette\Security,
    Nette\Utils\Strings;

class Authenticator extends Nette\Object implements Security\IAuthenticator {

    const
            TABLE_NAME = 'users',
            COLUMN_ID = 'id',
            COLUMN_NAME = 'username',
            COLUMN_PASSWORD = 'password',
            COLUMN_ROLE = 'role',
            PASSWORD_MAX_LENGTH = 4096
                ;

    private $employeeFacade;
    private $passwordSalt;

    public function __construct(Facade\BaseFacade $employeeFacade, $salt) {
        $this->employeeFacade = $employeeFacade;
        $this->passwordSalt = $salt;
    }

    /**
     * Performs an authentication.
     * @return Nette\Security\Identity
     * @throws Nette\Security\AuthenticationException
     */
    public function authenticate(array $credentials) {
        if (count($credentials) > 2) {
            list($login, $password, $extra) = $credentials;
        } else {
            list($login, $password) = $credentials;
            $extra = null;
        }

        $user = $this->employeeFacade->getOneByColumn("username", $login);

        if ($user === null) {
            throw new Security\AuthenticationException('Neplatný pokus o přihlášení - uživatel nenalezen.');
        }

        if ($extra == 99) {
            return new Security\Identity($user->id, $user->getRole());
        }

        if ($user->getPassword() != self::calculateHash($password, $this->passwordSalt)) {
            throw new Security\AuthenticationException('Neplatný pokus o přihlášení - nesprávné heslo.');
        }

        return new Nette\Security\Identity($user->id, $user->getRole());
    }
    
    public function getSalt() {
        return $this->passwordSalt;
    }

    /**
     * Computes salted password hash.
     * @param  string
     * @return string
     */
    public function calculateHash($password, $salt = NULL) {
        if ($password === Strings::upper($password)) { // perhaps caps lock is on
            $password = Strings::lower($password);
        }
        
        $password = substr($password, 0, self::PASSWORD_MAX_LENGTH);
        return crypt($password, $salt ? : $this->passwordSalt);
    }

}
